

// ---------------- FUNÇÃO PUXAR HEADER PARA PÁG --------------------------- FUNCIONA


// aparece e desaparece a caixa do user
$('.header').hover(function() {
	$('.jogadorC').show(100);
}, function() {
	$('.jogadorC').hide(100);
});

function reloadPag () {
	// não funciona - window.location.reload("../index.html");
	locationReload('../index.html');
}


// ---------------- FUNÇÃO BARRA PROGRESSO -------------------------------- FUNCIONA


function move(){
    var a = 100;
    setInterval(function(){
        a--;
       document.getElementById('barraProcesso').style.width = a+'%';
        //console.log(a);
    },200);
}


// ---------------- FUNÇÕES PÁG TUTORIAL ----------------------------------- FUNCIONA


// função para guardar o nome do utilizador
function guarda() {
	
	var a = document.getElementById('nome').value;
	//var b = $('#imgModal').val();
	let u = document.getElementById('imgModal');
	let c = u.files[0].name;

		
	localStorage.setItem("nome",a);
	localStorage.setItem("imgModal",c);

}


// ---------------- FUNÇÕES PÁG JOGAR ----------------------------------- FUNCIONA


// função resposta certa
function respostaCorreta(a) {
	// se a for 1 (correta)
	if(a==1){
		document.getElementById('btn2').style.backgroundColor = '#28a745';

		// pontuação
		var pontuacao = localStorage.getItem('pontuacao');
		pontuacao = Number(pontuacao);
		pontuacao = pontuacao + 15;
		localStorage.setItem('pontuacao',pontuacao);
		console.log(pontuacao);

		// escrever pontuação na pág das perguntas
		document.getElementById('pontos').innerHTML = pontuacao;

		// disable os btns depois de clicked
		$(".errada").prop('disabled', true);
		$("#btn2").prop('disabled', true);
	}
	else if(a==0) {
		var errada = document.getElementsByClassName('errada');
		/*for (var i = 0; i < errada.length; i++) {
			errada[i].style.backgroundColor = '#dc3545';
		}*/

		$('.errada').css('background-color', '#dc3545');
		pontuacao = pontuacao + 0;

		// disable os btns depois de clicked
		$(".errada").prop('disabled', true);
		$("#btn2").prop('disabled', true);
	}
}

/*const MAX_PONTOS = 225;
const MAX_PERGUNTAS = 15;*/
