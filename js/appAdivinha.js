

// ---------------- FUNÇÃO PUXAR HEADER PARA PÁG --------------------------- FUNCIONA


// aparece e desaparece a caixa do user
$('.header').hover(function(){
	$('.jogadorC').show(100);
}, function() {
	$('.jogadorC').hide(100);
});

function reloadPag(){
	// não funciona - window.location.reload("../index.html");
	locationReload('../index.html');
}


// ---------------- FUNÇÕES PÁG TUTORIAL ----------------------------------- FUNCIONA


// função para guardar o nome do utilizador
function guarda(){

	var a = document.getElementById('nome').value;
	//var b = $('#imgModal').val();
	let u = document.getElementById('imgModal');
	let c = u.files[0].name;

		
	localStorage.setItem("nome",a);
	localStorage.setItem("imgModal",c);

}


// ---------------- FUNÇÕES PÁG ADIVINHA ----------------------------------- FUNCIONA


var btnVerifica = document.getElementById('btnVerifica');
var numInserido = document.getElementById('numInserido');
var indicacao = document.getElementById('indicacao');

//var maxtentativas = 7;
// tentativa.innerHTML = "0";

var aleatorio;
document.getElementById('jogo').style.display = 'none';


// função faz desaparecer escolha níveis e aparece jogo
function desaparece(){
  document.getElementById('jogo').style.display = 'block';
  document.getElementById('escolheNivel').style.display = 'none';
}


// função para gerar nº aleatório, consoante o nível
function jogarAdivinha(b){
  if(b==0){
    // gerar o número aleatório Nível Fácil
    aleatorio = Math.round(Math.random() * 10);
    console.log(aleatorio);

    desaparece();

    document.getElementById('nomeNivel').innerHTML = "Nível Fácil";
    document.getElementById('nomeNivel').style.color = "green";
    document.getElementById('facilEscolha').innerHTML = "Escolhe um número <br> entre 0 e 10";
  }
  else if(b==1){
    // gerar o número aleatório Nível Médio
    aleatorio = Math.round(Math.random() * 50);
    console.log(aleatorio);

    desaparece();

    document.getElementById('nomeNivel').innerHTML = "Nível Médio";
    document.getElementById('nomeNivel').style.color = "green";
    document.getElementById('facilEscolha').innerHTML = "Escolhe um número <br> entre 0 e 50";
  }
  else if(b==2){
    // gerar o número aleatório Nível Difícil
    aleatorio = Math.round(Math.random() * 100);
    console.log(aleatorio);

    desaparece();

    document.getElementById('nomeNivel').innerHTML = "Nível Difícil";
    document.getElementById('nomeNivel').style.color = "green";
    document.getElementById('facilEscolha').innerHTML = "Escolhe um número <br> entre 0 e 100";
  }
}

// função do jogo em si para adivinhar (inclui nº tentativas)
function iniciaAdivinha(){

	if (numInserido.value==false) {
    indicacao.innerHTML = 'Digite algum valor';
    return indicacao;
  }

  var tentativa = document.getElementById('tentativa').innerHTML;
  tentativa = Number(tentativa);
	tentativa = tentativa+1;
	document.getElementById('tentativa').innerHTML= tentativa;

  if(aleatorio==numInserido.value)
  {
    indicacao.innerHTML = 'Parabéns, acertaste!!';
    indicacao.style.color = 'green';
    numInserido.disabled = true;
    //tentativa.value = "Número de tentativas: "+tentativa;
    btnVerifica.style.display = 'none';
    document.getElementById('novoJogo').style.display = 'block';
  }
  else if(aleatorio>numInserido.value)
  {
    indicacao.innerHTML = 'O número sorteado é maior';
    indicacao.style.color = 'red';
  }
  else if(aleatorio<numInserido.value)
  {
    indicacao.innerHTML = 'O número sorteado é menor';
    indicacao.style.color = 'red';
  } 	
}


// função para começar 1 novo jogo Adivinha número
function novoJogoAd(){
	window.location.href=window.location.href;
}

